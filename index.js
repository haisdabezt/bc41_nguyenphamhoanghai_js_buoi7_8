const arr = [];
// save input
const input = () => {
  var value = document.getElementById("soArr").value * 1;
  arr.push(value);
  document.getElementById("showArr").innerHTML = `👉 ${arr}`;
};
// Bài 1
const bai1 = () => {
  let tong = 0;
  const tongSoDuong = arr.filter(function (item) {
    if (item > 0) {
      tong += item;
    }
  });
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Tổng số dương: ${tong}`);
  return ketQua;
};

// Bài 2
const bai2 = () => {
  count = 0;
  const demSo = arr.filter(function (number) {
    if (number > 0) {
      count++;
    }
  });
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Số dương: ${count}`);
  return ketQua;
};

// Bài 3
const bai3 = () => {
  const min = Math.min(...arr);
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Số nhỏ nhất: ${min}`);
  return ketQua;
};

// Bài 4
const bai4 = () => {
  const newArr = arr.filter(function (item) {
    return item > 0;
  });
  const minDuong = Math.min(...newArr);
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Số dương nhỏ nhất: ${minDuong}`);
  return ketQua;
};

// Bài 5
const bai5 = () => {
  let ketQua;
  const newArr = arr.filter(function (item) {
    return item % 2 == 0;
  });
  if (newArr == 0) {
    ketQua = -1;
  } else {
    ketQua = Math.max(newArr[newArr.length - 1]);
  }
  var result = (document.getElementById(
    "result"
  ).innerHTML = `👉Số chẵn cuối cùng: ${ketQua}`);
  return result;
};

// Bài 6
const bai6 = () => {
  let vitri1 = document.getElementById("txt-vitri1").value * 1;
  let vitri2 = document.getElementById("txt-vitri2").value * 1;
  let assign = 0;
  if (vitri1 < 0 || vitri2 < 0) {
    alert("Chọn vị trí đổi (bắt đầu từ 0... )");
  } else if (vitri1 > arr.length - 1 || vitri2 > arr.length - 1) {
    alert("Vị trí lớn hơn index");
  } else {
    for (var i = 0; i < arr.length; i++) {
      if (vitri1 == i) {
        for (var j = 0; j < arr.length; j++) {
          if (vitri2 == j) {
            assign = arr[i];
            arr[i] = arr[j];
            arr[j] = assign;
          }
        }
      }
    }
  }
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Mảng sau khi đổi: ${arr}`);
  return ketQua;
};

// đổi chỗ
var chucNang = document.getElementById("yeuCau");
const change = document.getElementById("txt-doicho");
change.style.display = "none";
chucNang.onchange = function () {
  if (chucNang.value == 6) {
    change.style.display = "block";
    newArr_9.style.display = "none";
  } else if (chucNang.value == 9) {
    change.style.display = "none";
    newArr_9.style.display = "block";
  } else {
    change.style.display = "none";
    newArr_9.style.display = "none";
  }
};

// Bài 7
const bai7 = () => {
  const sortArr = arr.sort(function (a, b) {
    return a - b;
  });
  let ketQua = (document.getElementById(
    "result"
  ).innerHTML = `👉Mảng sau khi sắp xếp: ${sortArr}`);
  return ketQua;
};

// Bài 8
const bai8 = () => {
  let ketQua;
  const newArr = arr.filter(function (num) {
    if (num < 2) {
      return false;
    } else if (num == 2) {
      return true;
    } else {
      for (var i = 2; i < num; i++) {
        if (num % i === 0) {
          return false;
        }
      }
      return true;
    }
  });
  if (newArr == 0) {
    ketQua = document.getElementById("result").innerHTML = 0;
    return ketQua;
  } else {
    ketQua = document.getElementById(
      "result"
    ).innerHTML = `Số nguyên tố đầu tiên: ${newArr[0]} `;

    return ketQua;
  }
};

// Bài 9
const bai9 = () => {
  let tongArray = arr.concat(arr_9);
  let count = 0;
  let kiemTraTongArray = tongArray.filter(function (num) {
    if (Number.isInteger(num) == true) {
      count++;
    }
  });
  ketQua = document.getElementById(
    "result"
  ).innerHTML = `Tổng số nguyên: ${count}`;
  return ketQua;
};
// đếm số nguyên
const arr_9 = [];
const newArr_9 = document.getElementById("addArr_9");
newArr_9.style.display = "none";
document.getElementById("btn_9").onclick = function () {
  let inputBai9 = document.getElementById("addArr").value * 1;
  arr_9.push(inputBai9);
  document.getElementById("newArr_9").innerHTML += `${inputBai9}, `;
  console.log(arr_9);
};

// bài 10
const bai10 = () => {
  let soDuong = 0;
  let soAm = 0;
  let ketQua;
  const newArr = arr.filter(function (num) {
    if (num > 0) {
      soDuong++;
    }
    return soDuong;
  });
  const lastArr = arr.filter(function (item) {
    if (item < 0) {
      soAm++;
    }
    return soAm;
  });
  if (newArr > lastArr) {
    ketQua = document.getElementById("result").innerHTML = `Số dương > Số âm`;
  } else {
    ketQua = document.getElementById("result").innerHTML = `Số âm > Số dương`;
  }
  return ketQua;
};

const switchCase = () => {
  var chucNang = document.getElementById("yeuCau").value * 1;
  switch (chucNang) {
    case 1:
      {
        ketQua = bai1();
      }
      break;
    case 2:
      {
        ketQua = bai2();
      }
      break;
    case 3:
      {
        ketQua = bai3();
      }
      break;
    case 4:
      {
        ketQua = bai4();
      }
      break;
    case 5:
      {
        ketQua = bai5();
      }
      break;
    case 6:
      {
        ketQua = bai6();
      }
      break;
    case 7:
      {
        ketQua = bai7();
      }
      break;
    case 8:
      {
        ketQua = bai8();
      }
      break;
    case 9:
      {
        ketQua = bai9();
      }
      break;
    case 10:
      {
        ketQua = bai10();
      }
      break;
    default: {
      alert("Hãy chọn chức năng");
    }
  }
};
